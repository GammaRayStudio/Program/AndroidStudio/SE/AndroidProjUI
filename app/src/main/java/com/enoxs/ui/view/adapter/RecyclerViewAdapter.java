package com.enoxs.ui.view.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.enoxs.ui.R;

import java.util.List;

public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.ViewHolder> implements
        View.OnClickListener{
    private RecyclerViewAdapter.OnRecyclerViewItemClickListener mOnItemClickListener = null;

    private List<Integer> lstImage;
    private List<String> lstTitle;
    private List<String> lstSubtitle;

    public RecyclerViewAdapter(List<Integer> lstImage, List<String> lstTitle , List<String> lstSubtitle) {
        this.lstImage = lstImage;
        this.lstTitle = lstTitle;
        this.lstSubtitle = lstSubtitle;
    }

    @Override
    public RecyclerViewAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recyclerview_list_item, parent, false);
        RecyclerViewAdapter.ViewHolder viewHolder = new RecyclerViewAdapter.ViewHolder(view);

        view.setOnClickListener(this);

        return viewHolder;//連接布局，新增一個view給viewholder綁定元件
    }

    @Override
    public void onBindViewHolder(RecyclerViewAdapter.ViewHolder holder, int position) {
        holder.icon.setImageResource(lstImage.get(position));
        holder.title.setText(lstTitle.get(position));
        holder.subtitle.setText(lstSubtitle.get(position));

        holder.itemView.setTag(lstTitle.get(position)); // 點擊事件，取值的方法
    }

    @Override
    public int getItemCount() {
        return lstTitle.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private ImageView icon;
        private TextView title;
        private TextView subtitle;

        public ViewHolder(View Holder) {
            super(Holder);
            //取得從onCreateViewHolder的view，此ViewHolder綁定主布局元件
            icon = Holder.findViewById(R.id.icon);
            title = Holder.findViewById(R.id.title);
            subtitle = Holder.findViewById(R.id.subtitle);
        }
    }


    /**
     * RecyclerView.onClickEvent
     */

    public interface OnRecyclerViewItemClickListener {
        void onItemClick(View view, String data);
    }

    @Override
    public void onClick(View v) {
        if (mOnItemClickListener != null) {
            mOnItemClickListener.onItemClick(v, (String) v.getTag());
        }
    }

    public void setOnItemClickListener(RecyclerViewAdapter.OnRecyclerViewItemClickListener listener) {
        this.mOnItemClickListener = listener;
    }
}
