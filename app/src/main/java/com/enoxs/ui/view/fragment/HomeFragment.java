package com.enoxs.ui.view.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.navigation.fragment.NavHostFragment;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.enoxs.ui.R;
import com.enoxs.ui.view.adapter.HomeAdapter;
import com.enoxs.ui.databinding.FragmentHomeBinding;

import java.util.Arrays;
import java.util.List;
import java.util.logging.Logger;

public class HomeFragment extends Fragment {
    private Logger log = Logger.getLogger(this.getClass().getName());

    private FragmentHomeBinding binding;

    private List<String> lstTitle;
    private RecyclerView recyclerView;
    private HomeAdapter homeAdapter;

    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState
    ) {

        binding = FragmentHomeBinding.inflate(inflater, container, false);
        return binding.getRoot();
    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        lstTitle = Arrays.asList("Fragment" , "RecyclerView");

        homeAdapter = new HomeAdapter(lstTitle);//將資料進行處理

        recyclerView = binding.recyclerview;
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.addItemDecoration(new DividerItemDecoration(getContext(),
                DividerItemDecoration.VERTICAL));
        recyclerView.setAdapter(homeAdapter);

        homeAdapter.setOnItemClickListener(new HomeAdapter.OnRecyclerViewItemClickListener() {
            @Override
            public void onItemClick(View view, String data) {
                switch (data){
                    case "Fragment":
                        NavHostFragment.findNavController(HomeFragment.this)
                                .navigate(R.id.toSecondFragment);
                        break;
                    case "RecyclerView":
                        NavHostFragment.findNavController(HomeFragment.this)
                                .navigate(R.id.toRecyclerViewFragment);
                        break;
                }

            }
        });
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }

}