package com.enoxs.ui.view.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.enoxs.ui.R;

import java.util.List;

public class HomeAdapter extends RecyclerView.Adapter<HomeAdapter.ViewHolder> implements
        View.OnClickListener{
    private OnRecyclerViewItemClickListener mOnItemClickListener = null;

    private List<String> lstTitle;

    public HomeAdapter(List<String> lstTitle) {
        this.lstTitle = lstTitle;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.home_list_item, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);

        view.setOnClickListener(this);

        return viewHolder;//連接布局，新增一個view給viewholder綁定元件
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.title.setText(lstTitle.get(position));//position為索引值，用get來取得arraylist資料
        holder.itemView.setTag(lstTitle.get(position));
    }

    @Override
    public int getItemCount() {
        return lstTitle.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView title;

        public ViewHolder(View Holder) {
            super(Holder);
            //取得從onCreateViewHolder的view，此ViewHolder綁定主布局元件
            title = Holder.findViewById(R.id.txtTitle);
        }
    }


    /**
     * RecyclerView.onClickEvent
     */

    public interface OnRecyclerViewItemClickListener {
        void onItemClick(View view, String data);
    }

    @Override
    public void onClick(View v) {
        if (mOnItemClickListener != null) {
            mOnItemClickListener.onItemClick(v, (String) v.getTag());
        }
    }

    public void setOnItemClickListener(OnRecyclerViewItemClickListener listener) {
        this.mOnItemClickListener = listener;
    }
}