package com.enoxs.ui.view.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.enoxs.ui.R;
import com.enoxs.ui.databinding.FragmentRecyclerviewBinding;
import com.enoxs.ui.view.adapter.RecyclerViewAdapter;

import java.util.Arrays;
import java.util.List;
import java.util.logging.Logger;

public class RecyclerViewFragment extends Fragment {
    private Logger log = Logger.getLogger(this.getClass().getName());

    private FragmentRecyclerviewBinding binding;

    private List<String> lstTitle;
    private List<String> lstSubtitle;
    private List<Integer> lstImage;
    private RecyclerView recyclerView;
    private RecyclerViewAdapter recyclerViewAdapter;

    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState
    ) {

        binding = FragmentRecyclerviewBinding.inflate(inflater, container, false);
        return binding.getRoot();

    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        lstImage = Arrays.asList(R.drawable.item_icon_01, R.drawable.item_icon_02, R.drawable.item_icon_03);
        lstTitle = Arrays.asList("Title01" , "Title02" , "Title03");
        lstSubtitle = Arrays.asList("Subtitle01" , "Subtitle02" , "Subtitle03");


        recyclerViewAdapter = new RecyclerViewAdapter(lstImage , lstTitle , lstSubtitle);//將資料進行處理

        recyclerView = binding.recyclerview;

        recyclerView.getItemAnimator().setMoveDuration(200);

        // 表格佈局
        GridLayoutManager gridManager = new GridLayoutManager(getContext() , 2);
        LinearLayoutManager lineManager = new LinearLayoutManager(getContext());

        recyclerView.setLayoutManager(lineManager);
//        recyclerView.setLayoutManager(gridManager);

        recyclerView.addItemDecoration(new DividerItemDecoration(getContext(),
                DividerItemDecoration.VERTICAL));
        recyclerView.setAdapter(recyclerViewAdapter);

        recyclerViewAdapter.setOnItemClickListener(new RecyclerViewAdapter.OnRecyclerViewItemClickListener() {
            @Override
            public void onItemClick(View view, String data) {
                log.info("onItemClick().");
                log.info("data => " + data);
            }
        });
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }

}